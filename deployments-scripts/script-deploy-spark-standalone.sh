#!/bin/bash

# Set environment variables
echo "export SPARK_HOME=/usr/local/spark" >> ~/.bashrc;
echo "export PATH=$PATH:$SPARK_HOME/bin" >> ~/.bashrc;
echo "export USER_PATH_CUSTOM=/home/ubuntu" >> ~/.bashrc;
source ~/.bashrc;

# Update package manager
sudo apt update -y
echo "APT update OK "

# Install Java
sudo apt install openjdk-11-jdk git maven -y
echo "APT install OK "

# Install Spark

wget -P "$USER_PATH_CUSTOM" https://dlcdn.apache.org/spark/spark-3.3.1/spark-3.3.1-bin-hadoop3-scala2.13.tgz;

echo "WGET OK ";

cd "$USER_PATH_CUSTOM" && tar xvf spark-3.3.1-bin-hadoop3-scala2.13.tgz;
echo "TAR OK ";
cd "$USER_PATH_CUSTOM" && sudo mkdir /usr/local/spark/ && sudo mv spark-3.3.1-bin-hadoop3-scala2.13.tgz ./usr/local/spark/;
echo "MV OK";


echo "BASHRC OK ";

cd "$USER_PATH_CUSTOM" && git clone https://github.com/SimonRousseaud/ExploitationDonnees.git ExploitationDonnees
echo "GIT CLONED OK "
# Deploy your application
cd "$USER_PATH_CUSTOM" && cd "ExploitationDonnees"  && mvn install
echo "MVN Install OK "

ip4=$(/sbin/ip -o -4 addr list ens33 | awk '{print $4}' | cut -d/ -f1)
echo "IP4 : "
echo "$ip4"

start-worker.sh spark://"$ip4":7077

# Execute the application
spark-submit --class org.example.main --master local[*] /root/.m2/repository/org/example/Projet-javaaaaa3/1.0-SNAPSHOT/Projet-javaaaaa3-1.0-SNAPSHOT.jar



