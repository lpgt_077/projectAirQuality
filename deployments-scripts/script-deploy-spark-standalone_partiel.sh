#!/bin/bash

# Set environment variables
if ! grep -q "SPARK_HOME" /home/ubuntu/.bashrc; then
    echo "export SPARK_HOME=/usr/local/spark" >> /home/ubuntu/.bashrc
    source /home/ubuntu/.bashrc;

fi

if ! grep -q "PATH.*SPARK_HOME" /home/ubuntu/.bashrc; then
    echo "export PATH=\$PATH:\$SPARK_HOME/bin" >> /home/ubuntu/.bashrc
fi

if ! grep -q "USER_PATH_CUSTOM" /home/ubuntu/.bashrc; then
    echo "export USER_PATH_CUSTOM=/home/ubuntu" >> /home/ubuntu/.bashrc
fi

if ! grep -q "HADOOP_HOME" /home/ubuntu/.bashrc; then
    echo "export HADOOP_HOME=/home/ubuntu/hadoop-3.3.4" >> /home/ubuntu/.bashrc
    source /home/ubuntu/.bashrc;
    echo "export HADOOP_INSTALL=/home/ubuntu/hadoop-3.3.4" >> /home/ubuntu/.bashrc
    echo "export HADOOP_MAPRED_HOME=/home/ubuntu/hadoop-3.3.4" >> /home/ubuntu/.bashrc
    echo "export HADOOP_COMMON_HOME=/home/ubuntu/hadoop-3.3.4" >> /home/ubuntu/.bashrc
    echo "export HADOOP_HDFS_HOME=/home/ubuntu/hadoop-3.3.4" >> /home/ubuntu/.bashrc
    echo "export YARN_HOME=/home/ubuntu/hadoop-3.3.4" >> /home/ubuntu/.bashrc
    echo "export HADOOP_COMMON_LIB_NATIVE_DIR=/home/ubuntu/hadoop-3.3.4/lib/native" >> /home/ubuntu/.bashrc
    echo "export PATH=\$PATH:/home/ubuntu/hadoop-3.3.4/sbin:/home/ubuntu/hadoop-3.3.4/bin" >> /home/ubuntu/.bashrc
    echo "export HADOOP_OPTS=/home/ubuntu/hadoop-3.3.4/lib/native" >> /home/ubuntu/.bashrc
fi
source /home/ubuntu/.bashrc;

# Set environment variables
if ! grep -q "SPARK_HOME" ~/.bashrc; then
    echo "export SPARK_HOME=/usr/local/spark" >> ~/.bashrc
    source ~/.bashrc;

fi

if ! grep -q "PATH.*SPARK_HOME" ~/.bashrc; then
    echo "export PATH=\$PATH:\$SPARK_HOME/bin" >> ~/.bashrc
fi

if ! grep -q "USER_PATH_CUSTOM" ~/.bashrc; then
    echo "export USER_PATH_CUSTOM=/home/ubuntu" >> ~/.bashrc
fi

if ! grep -q "HADOOP_HOME" ~/.bashrc; then
    echo "export HADOOP_HOME=/home/ubuntu/hadoop-3.3.4" >> ~/.bashrc
    source ~/.bashrc;
    echo "export HADOOP_INSTALL=/home/ubuntu/hadoop-3.3.4" >> ~/.bashrc
    echo "export HADOOP_MAPRED_HOME=/home/ubuntu/hadoop-3.3.4" >> ~/.bashrc
    echo "export HADOOP_COMMON_HOME=/home/ubuntu/hadoop-3.3.4" >> ~/.bashrc
    echo "export HADOOP_HDFS_HOME=/home/ubuntu/hadoop-3.3.4" >> ~/.bashrc
    echo "export YARN_HOME=/home/ubuntu/hadoop-3.3.4" >> ~/.bashrc
    echo "export HADOOP_COMMON_LIB_NATIVE_DIR=/home/ubuntu/hadoop-3.3.4/lib/native" >> ~/.bashrc
    echo "export PATH=\$PATH:/home/ubuntu/hadoop-3.3.4/sbin:/home/ubuntu/hadoop-3.3.4/bin" >> ~/.bashrc
    echo "export HADOOP_OPTS=/home/ubuntu/hadoop-3.3.4/lib/native" >> ~/.bashrc
fi
source ~/.bashrc;

# Update package manager
sudo apt update -y
echo "APT update OK "

# Install Java
sudo apt install openjdk-11-jdk git maven sshpass -y
echo "APT install OK "

cd hadoop-3.3.4/

echo "export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64" >> /home/ubuntu/hadoop-3.3.4/etc/hadoop/hadoop-env.sh;
source /home/ubuntu/hadoop-3.3.4/etc/hadoop/hadoop-env.sh;

# END NEW

# Install SSH

apt install openssh-server openssh-client -y
sshpass -p 'linuxvmimages.com' ssh root@localhost

ip4=$(/sbin/ip -o -4 addr list ens33 | awk '{print $4}' | cut -d/ -f1)
echo "IP4 : "
echo "$ip4"

if ! grep -q "export HDFS_NAMENODE_USER=root" /home/ubuntu/hadoop-3.3.4/etc/hadoop/hadoop-env.sh; then
    echo "export HDFS_NAMENODE_USER=root" >> /home/ubuntu/hadoop-3.3.4/etc/hadoop/hadoop-env.sh
    echo "export HDFS_NAMENODE_USER=root" >> /home/ubuntu/hadoop-3.3.4/etc/hadoop/hadoop-env.sh
    echo "export HDFS_DATANODE_USER=root" >> /home/ubuntu/hadoop-3.3.4/etc/hadoop/hadoop-env.sh
    echo "export HDFS_SECONDARYNAMENODE_USER=root" >> /home/ubuntu/hadoop-3.3.4/etc/hadoop/hadoop-env.sh
    echo "export YARN_RESOURCEMANAGER_USER=root" >> /home/ubuntu/hadoop-3.3.4/etc/hadoop/hadoop-env.sh
    echo "export YARN_NODEMANAGER_USER=root" >> /home/ubuntu/hadoop-3.3.4/etc/hadoop/hadoop-env.sh

fi

    source /home/ubuntu/hadoop-3.3.4/etc/hadoop/hadoop-env.sh

echo "STARTCAT"
echo "$(cat /home/ubuntu/ExploitationDonnees/config/core-site.xml)" > /home/ubuntu/hadoop-3.3.4/etc/hadoop/core-site.xml
echo "$(cat /home/ubuntu/ExploitationDonnees/config/hdfs-site.xml)" > /home/ubuntu/hadoop-3.3.4/etc/hadoop/hdfs-site.xml
echo "$(cat /home/ubuntu/ExploitationDonnees/config/mapred-site.xml)" > /home/ubuntu/hadoop-3.3.4/etc/hadoop/mapred-site.xml
echo "$(cat /home/ubuntu/ExploitationDonnees/config/yarn-site.xml)" > /home/ubuntu/hadoop-3.3.4/etc/hadoop/yarn-site.xml
echo "STARTBASH"

cd /home/ubuntu && cd hadoop-3.3.4 && bin/hdfs namenode -format
cd /home/ubuntu && cd hadoop-3.3.4 && sbin/start-dfs.sh
cd /home/ubuntu && cd hadoop-3.3.4 && sbin/start-yarn.sh
start-worker.sh spark://"$ip4":7077

# Execute the application
spark-submit --class org.example.main --master spark://localhost:7077 --driver-library-path /home/ubuntu/hadoop-3.3.4/lib/native --jars "/home/ubuntu/hadoop-3.3.4/lib/*,$SPARK_HOME/jars/*" target/Projet-javaaaaa3-1.0-SNAPSHOT.jar
