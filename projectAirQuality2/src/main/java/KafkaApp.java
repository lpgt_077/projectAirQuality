import Kafka.KafkaReceiver;
import Writer.CsvWriter;
import beans.Mesure;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

import java.io.IOException;
import java.util.List;

@Slf4j
public class KafkaApp {
    public static void main(String[] args) throws IOException, InterruptedException {

        Config config = ConfigFactory.load("application.conf");

        String masterUrl = config.getString("3il.master");
        String appNAme = config.getString("3il.name");

        String inputPath = config.getString("3il.path.input");
        String outputPath = config.getString("3il.path.output");
        String checkPoint = config.getString("3il.path.checkPoint");
        List<String> topics = config.getStringList("3il.kafka.list");


        SparkSession sparkSession = SparkSession.builder().master(masterUrl).appName(appNAme).getOrCreate();

        FileSystem hdfs = FileSystem.get(sparkSession.sparkContext().hadoopConfiguration());

        JavaStreamingContext javaStreamingContext = JavaStreamingContext.getOrCreate(
                checkPoint,
                ()->{
                    JavaStreamingContext jsc = new JavaStreamingContext(
                            JavaSparkContext.fromSparkContext(sparkSession.sparkContext()),
                            new Duration(1000 * 10)
                    );
                    jsc.checkpoint(checkPoint);


                    KafkaReceiver receiver = new KafkaReceiver (topics,jsc);
                    JavaDStream<String> mesureJavaDStream = receiver.get();

                    mesureJavaDStream.foreachRDD(
                            mesureJavaRDD -> {
                                long ts = System.currentTimeMillis();
                                log.info("batch at {}", ts);
                                Dataset<String> mesureDataset = SparkSession.active().createDataset(
                                        mesureJavaRDD.rdd(),
                                        Encoders.bean(String.class)
                                ).cache();

                                mesureDataset.printSchema();
                                mesureDataset.show(5,false);
                                log.info("count {}",mesureDataset.count());

                                CsvWriter writer = new CsvWriter(outputPath +"/time"+ ts);
                                writer.accept(mesureDataset);
                                mesureDataset.unpersist();
                            }
                    );

                    return jsc;
                },
                sparkSession.sparkContext().hadoopConfiguration()
        );

        javaStreamingContext.start();
        javaStreamingContext.awaitTermination();

    }
}
