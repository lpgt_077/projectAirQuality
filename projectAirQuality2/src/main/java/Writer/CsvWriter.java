package Writer;

import beans.Mesure;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Dataset;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class CsvWriter implements Consumer<Dataset<String>> {

    private  final String outputPath ;

    @Override
    public void accept(Dataset<String> prixDataset) {
        prixDataset.write().csv(outputPath);
    }
}
