package processor;

import Writer.CsvWriter;
import beans.Mesure;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.VoidFunction2;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.Time;


@Slf4j
@RequiredArgsConstructor
public class airQualityProcessor implements VoidFunction2<JavaRDD<String>, org.apache.spark.streaming.Time> {

    private final SparkSession sparkSession;
    private final String outputPath;

    @Override
    public void call(JavaRDD<String> mesureJavaRDD, Time time) throws Exception {
        long ts = System.currentTimeMillis();
        log.info("micro-batch time={} at store in folder time={}",time,ts);

        if (mesureJavaRDD.isEmpty()){
            log.info("No data found!");
            return;
        }

        log.info("create dataset from rdd");
        Dataset<String> mesureDataset = sparkSession.createDataset(
                mesureJavaRDD.rdd(),
                Encoders.bean(String.class)
        ).cache();

        log.info("affiche quelques lignes du dataset");
        mesureDataset.printSchema();
        mesureDataset.show(5,false);

        log.info("nb Mesure={}",mesureDataset.count());

        CsvWriter writer = new CsvWriter(outputPath +"/time"+ ts);
        writer.accept(mesureDataset);

        mesureDataset.unpersist();
        log.info("end of program");

    }
}
