package receiver;

import beans.Mesure;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import types.*;

import java.util.function.Supplier;

@RequiredArgsConstructor
@Slf4j
public class airQualityReceiver implements Supplier<JavaDStream> {

    private final String inputPath;
    private final JavaStreamingContext jsc;


    TextToMesure ttm = new TextToMesure();

    private Function<String, Mesure> mapper = ttm::call;

    private final Function<Path,Boolean> filter = path -> path.getName().endsWith(".csv");

    @Override
    public JavaDStream get() {

        JavaPairInputDStream<airQualityLongWritable, airQualityText> inputStream = jsc
                .fileStream(
                        inputPath,
                        airQualityLongWritable.class,
                        airQualityText.class,
                        airQualityFileInputFormat.class,
                        filter,
                        true
                );
        return inputStream.map(t ->t._2().toString()).map(mapper);

    }
}

