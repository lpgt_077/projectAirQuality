package types;


import beans.Mesure;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.function.Function;

public class TextToMesure implements Function<String, Mesure> {
    @Override
    public Mesure call(String s) throws Exception {

        String[] data = StringUtils.splitByWholeSeparatorPreserveAllTokens(s,";",2);

        String X =   data[0];
        String Y =  data[1] ;
        String nom_dept = data[2] ;
        String nom_com = data[3] ;
        String insee_com = data[4] ;
        String nom_station = data[5] ;
        String code_station = data[6] ;
        String typologie = data[7] ;
        String influence = data[8] ;
        String nom_poll = data[9];
        String id_poll_ue = data[10];
        String valeur = data[11];
        String unite = data[12];
        String metrique = data[13];
        String date_debut = data[14];
        String date_fin = data[15];
        String statut_valid = data[16];
        String x_wgs84 =data[17];
        String y_wgs84 = data[18] ;
        String y_l93 = data[19] ;
        String x_l93 = data[20];
        String the_geom = data[21];
        String id = data[22];
        String ObjectId = data[23];

        return Mesure.builder()
                .X(X)
                .Y(Y)
                .nom_dept(nom_dept)
                .nom_com(nom_com)
                .insee_com(insee_com)
                .nom_station(nom_station)
                .code_station(code_station)
                .typologie(typologie)
                .influence(influence)
                .nom_poll(nom_poll)
                .id_poll_ue(id_poll_ue)
                .valeur(valeur)
                .unite(unite)
                .metrique(metrique)
                .date_debut(date_debut)
                .date_fin(date_fin)
                .statut_valid(statut_valid)
                .x_wgs84(x_wgs84)
                .y_wgs84(y_wgs84)
                .y_l93(y_l93)
                .x_l93(x_l93)
                .the_geom(the_geom)
                .id(id)
                .ObjectId(ObjectId)
                .build();
    }
}
