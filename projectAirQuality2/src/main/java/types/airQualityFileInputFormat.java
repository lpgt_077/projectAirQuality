package types;

import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class airQualityFileInputFormat extends FileInputFormat<airQualityLongWritable, airQualityText> {
    @Override
    public RecordReader<airQualityLongWritable, airQualityText> createRecordReader(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        String delimiter = context.getConfiguration().get("textinputformat.record.delimiter");

        byte[] recordDelimiterBytes =  null;
        if (null!=delimiter){
            recordDelimiterBytes = delimiter.getBytes(StandardCharsets.UTF_8);
        }

        return new airQualityLineRecordReader(recordDelimiterBytes);
    }
}
