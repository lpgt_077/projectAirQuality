package unitaires;



import org.apache.spark.sql.Row;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.testng.annotations.Test;
import types.RowToMesure;

public class RowToMesureTestUnitaire {

    @Test
    public void testRowToActeDecesFunc() {

        RowToMesure f = new RowToMesure();
        StructType schema = new StructType(
                new StructField[]{
                        new StructField(
                                "X",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),

                        new StructField(
                                "Y",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),

                        new StructField(
                                "nom_dept",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),
                        new StructField(
                                "nom_com",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "insee_com",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "nom_station",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "code_station",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "typologie",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "influence",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "nom_poll",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "id_poll_ue",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "valeur",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "unite",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "metrique",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "date_debut",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "date_fin",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "statut_valid",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "x_wgs84",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "y_wgs84",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ),  new StructField(
                                "y_l93",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ), new StructField(
                                "x_l93",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ), new StructField(
                                "the_geom",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ), new StructField(
                                "id",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        ), new StructField(
                                "ObjectId",
                                DataTypes.StringType,
                                true,
                                Metadata.empty()

                        )

                }
        );
        // valeur 1ere ligne fichier csv
        String values[] = {"1.69313","46.798267","Indre","Châteauroux","36044","Chateauroux Sud","FR34051",
                "Urbaine","Fond","NO"
                ,"38","0","µg/m3","horaire",
                "2023/01/09 11:00:00+00"
                ,"2023/01/09 11:59:59+00"
                ,"0","1.69313","46.798267","6 633 950,0203331","600 329,2082205",
                "0101000020E610000010069E7B0F17FB3FEF3CF19C2D664740","1","1"
        } ;

        Row row = new GenericRowWithSchema(values , schema);
        Mesure expected = Mesure.builder()
                .X(values[0]!= null ?  values[0] : "")
                // todo : fix index ==> test ne peux reussir
                .Y("")
                .nom_dept(values[1]!= null ?  values[1] : "")
                .nom_com(values[2]!= null ?  values[2] : "")
                .insee_com(values[3]!= null ?  values[3] : "")
                .nom_station(values[4]!= null ?  values[4] : "")
                .code_station(values[5]!= null ?  values[5] : "")
                .typologie(values[6]!= null ?  values[6] : "")
                .influence(values[7]!= null ?  values[7] : "")
                .nom_poll(values[8]!= null ?  values[8] : "")
                .id_poll_ue(values[9]!= null ?  values[9] : "")
                .valeur(values[10]!= null ?  values[10] : "")
                .unite(values[11]!= null ?  values[11] : "")
                .metrique(values[12]!= null ?  values[12] : "")
                .date_debut(values[13]!= null ?  values[13] : "")
                .date_fin(values[14]!= null ?  values[14] : "")
                .statut_valid(values[15]!= null ?  values[15] : "")
                .x_wgs84(values[16]!= null ?  values[16] : "")
                .y_wgs84(values[17]!= null ?  values[17] : "")
                .y_l93(values[18]!= null ?  values[18] : "")
                .x_l93(values[19]!= null ?  values[19] : "")
                .the_geom(values[20]!= null ?  values[20] : "")
                .id(values[21]!= null ?  values[21] : "")
                .ObjectId(values[22]!= null ?  values[22] : "")
                .build();

        Mesure actual = f.apply(row);


        assertThat(actual).isEqualTo(expected);

    }
}

