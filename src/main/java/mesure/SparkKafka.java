package mesure;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.codehaus.jackson.map.deser.std.StringDeserializer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Slf4j
@RequiredArgsConstructor
public class KafkaReceiver {
    private final List<String> topics;
    private final JavaStreamingContext jsc;
    private final Map<String, Object> kafkaParams = new HashMap<String, Object>() {{
        put("bootstrap.servers", "localhost:9092");
        put("key.deserializer", StringDeserializer.class);
        put("value.deserializer", StringDeserializer.class);
        put("group.id", "spark-kafka-integ");
        put("auto.offset.reset", "earliest");
    }};
    public JavaDStream<String> get() {
        JavaInputDStream<ConsumerRecord<String, String>> directStream = KafkaUtils.createDirectStream(
                jsc,
                LocationStrategies.PreferConsistent(),
                ConsumerStrategies.Subscribe(topics, kafkaParams)
        );
        JavaDStream<String> javaDStream = directStream.map(ConsumerRecord::value);
        return JavaDStream;
    }
}