package mesure.beans;

import java.io.Serializable;

import lombok.*;

@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Builder
@Data
public class Mesure implements Serializable {

    private String X;
    private String Y;
    private String nom_dept;
    private String nom_com;
    private String insee_com;
    private String nom_station;
    private String code_station;
    private String typologie;
    private String influence;
    private String nom_poll;
    private String id_poll_ue;
    private String valeur;
    private String unite;
    private String metrique;
    private String date_debut;
    private String date_fin;
    private String statut_valid;
    private String x_wgs84;
    private String y_wgs84;
    private String y_l93;
    private String x_l93;
    private String the_geom;
    private String id;
    private String ObjectId;


}