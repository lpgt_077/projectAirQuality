package mesure.functions;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import mesure.beans.Mesure;

import java.util.function.Function;


public class MesureMapper implements Function<Dataset<Row>, Dataset<Mesure>> {
    private final RowToMesure parser = new RowToMesure();
    private final MapFunction<Row, Mesure> task = parser::apply;

    @Override
    public Dataset<Mesure> apply(Dataset<Row> inputDS)
    {
        return inputDS.map(task , Encoders.bean(Mesure.class)) ;
    }
}
