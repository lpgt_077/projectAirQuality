package mesure.functions;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import mesure.beans.Mesure;

import java.util.function.Function;

public class MesureStatFunction implements Function<Dataset<Row>, Dataset<Mesure> >{
    @Override
    public Dataset<Mesure> apply(Dataset<Row> rowDataset)
    {
        // transformation du dataset de row en dataset d'objet --> mesure
        Dataset<Mesure> cleanDs = new MesureMapper().apply(rowDataset);
      //  cleanDs.show(30, false);
        return cleanDs;
    }
}
