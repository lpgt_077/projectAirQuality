package mesure.functions;

import org.apache.spark.sql.Row;
import mesure.beans.Mesure;

import java.io.Serializable;
import java.util.function.Function;

public class RowToMesure implements Function<Row, Mesure> , Serializable {
    @Override
    public Mesure apply(Row row)
    {


        String X =   row.getAs("X");
        String Y = row.getAs("Y") ;
        String nom_dept = row.getAs("nom_dept") ;
        String nom_com = row.getAs("nom_com") ;
        String insee_com = row.getAs("insee_com") ;
        String nom_station = row.getAs("nom_station") ;
        String code_station = row.getAs("code_station") ;
        String typologie = row.getAs("typologie") ;
        String influence = row.getAs("influence") ;
        String nom_poll = row.getAs("nom_poll") ;
        String id_poll_ue = row.getAs("id_poll_ue") ;
        String valeur = row.getAs("valeur") ;
        String unite = row.getAs("unite") ;
        String metrique = row.getAs("metrique") ;
        String date_debut = row.getAs("date_debut") ;
        String date_fin = row.getAs("date_fin") ;
        String statut_valid = row.getAs("statut_valid") ;
        String x_wgs84 = row.getAs("x_wgs84") ;
        String y_wgs84 = row.getAs("y_wgs84") ;
        String y_l93 = row.getAs("y_l93") ;
        String x_l93 = row.getAs("x_l93") ;
        String the_geom = row.getAs("the_geom") ;
        String id = row.getAs("id") ;
        String ObjectId = row.getAs("ObjectId") ;

        return Mesure.builder()
                .X(X)
                .Y(Y)
                .nom_dept(nom_dept)
                .nom_com(nom_com)
                .insee_com(insee_com)
                .nom_station(nom_station)
                .code_station(code_station)
                .typologie(typologie)
                .influence(influence)
                .nom_poll(nom_poll)
                .id_poll_ue(id_poll_ue)
                .valeur(valeur)
                .unite(unite)
                .metrique(metrique)
                .date_debut(date_debut)
                .date_fin(date_fin)
                .statut_valid(statut_valid)
                .x_wgs84(x_wgs84)
                .y_wgs84(y_wgs84)
                .y_l93(y_l93)
                .x_l93(x_l93)
                .the_geom(the_geom)
                .id(id)
                .ObjectId(ObjectId)
                .build();
    }
}
