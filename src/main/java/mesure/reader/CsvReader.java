package mesure.reader;

import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.function.Supplier;

@RequiredArgsConstructor
public class CsvReader  implements Supplier<Dataset<Row>> {

    private  final SparkSession sparkSession ;
    private  final String inputPath ;

    @SneakyThrows
    @Override
    public Dataset<Row> get()
    {
        FileSystem hdfs = FileSystem.get(new Configuration());
        Path pathInput = new Path(inputPath);

        if (hdfs.exists(pathInput))
        {
            FileStatus[] listFiles = hdfs.listStatus(pathInput);
            String[] inputPaths = Arrays.stream(listFiles)
                    .filter(t -> !t.isDirectory())
                    .map(f -> f.getPath().toString())
                    .toArray(String[]::new);
            Dataset<Row> inputDS = spark.read().option("delimiter", ";")
            .option( "header", "true").csv(inputPaths);
            inputDS.show(5, false);
            return inputDS;
        }
        return spark.emptyDataFrame();

    }


/*
        Dataset<Row> ds = sparkSession.read().option("delimiter" , ",")
                .option("header" , "true").csv(inputPath) ;
        ds.show(5, false);
        ds.printSchema();

        return ds;

    }
    */

}
