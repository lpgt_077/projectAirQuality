package mesure.writer;

import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Dataset;
import mesure.beans.Mesure;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class CsvWriter implements Consumer<Dataset<Mesure>> {

    private  final String outputPath ;

    @Override
    public void accept(Dataset<Mesure> prixDataset) {
        prixDataset.write().csv(outputPath);
    }
}
